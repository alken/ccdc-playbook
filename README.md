# Kenny’s Splunk Playbook (Rough Draft)

This is a playbook I wrote to prepare myself for CCDC. This guide applies to Splunk 8.1.1 CentOS 7 VM Playbook. This guide is based on Cord’s CentOS Write-up.


## Table of Contents
Note: Document every performed action listed under “Preparations” and “Possible Injects” to the CCJ (and CC Form when necessary) and document every situation listed under the “What To Do If...” topic to the IR form.

### Preparations
1. Change default passwords
i. For the system (CentOS 7)
ii. For the dedicated service (Splunk 8.1.1)
2. Remove unnecessary users/groups
i. For the system (CentOS 7)
ii. For the dedicated service (Splunk 8.1.1)
3. Configure sudoers
4. Configure login definitions
5. Install/update packages and necessary repositories
6. Set up optimal CLI environment with tmux
i. Continuously monitor processes/services in a separate tmux session (htop)
ii. Continuously monitor network traffic in a separate tmux session (nethogs)
iii. Continuously monitor important logs in a separate tmux session (multitail or tail -f)
7. Stop/disable unnecessary processes
8. Install and use UFW
9. Lock down critical files
10. Adjust NTP to time.gov

### Possible Injects
11. Update password policy
i. For the system (CentOS 7)
ii. For the dedicated service (Splunk 8.1.1)
12. Add, enable, and unlock users/groups
i. For the system (CentOS 7)
ii. For the dedicated service (Splunk 8.1.1)
13. Install and use ClamAV
14. Install and use Nmap
15. Enable/disable SSH

### What To Do If...
16. There is a rogue SSH connection
17. There is a malicious process running
18. There is suspicious network traffic
19. A vital component is nonfunctional
i. For the system (CentOS 7)
ii. For the dedicated service (Splunk 8.1.1)
20. Malware has been detected

## Preparations
### 1. Change default passwords

First things first, the basics must be covered.

i. For the system (CentOS 7)

`cat /etc/passwd – Display all users and user details`

`awk -F':' '{ print $1}' /etc/passwd – Display all users only`

`passwd <username> - Change password for given username`

`passwd -l <username> - Lock an account for given username`

ii. For the dedicated service (Splunk 8.1.1)

`./splunk list user – Display all users and user details`

`./splunk edit user <username> -auth admin:<admin_password> -password <password> - Change a password for given username (changing the password automatically locks the local user account)`

To lock a LDAP user, create a local account for that user. This will override the LDAP account. (See Section 12.ii)

### 2. Remove unnecessary users/groups

Get rid of users and groups that are not included on the whitelist.

i. For the system (CentOS 7)

`cat /etc/group – Display all groups and group details`

`lid <username> – Show all groups that the given user is associated with`

`lid -g <group_name> – Show all users that the given group contains`

`userdel -rf <username> - Delete a user and the respective home directory`

`delgroup <group_name> - Delete a group (cannot be the primary group of any user)`

`chage -E0 <username> - Disable a user account`

ii. For the dedicated service (Splunk 8.1.1)

`./splunk list role – Display all roles and role details`

`./splunk remove user <username> -auth admin:<admin_password> - Delete a local user`

To delete a LDAP user, the user most be removed from the Splunk active directory group. Contact an AD administrator for assistance.

### 3. Configure sudoers

Make sure there is absolutely no way for anyone to gain any root privileges without entering the root password.

`grep ‘NOPASSWD’ /etc/sudoers – The first check for the NOPASSWD tag`

`grep ‘NOPASSWD’ /etc/sudoers.d – The second check for the NOPASSWD tag`

If there is output for either or both of the commands above, then the respective files will have to be modified. It may be safer to edit the files to manually remove all lines found in the output of both of the commands above than to execute the following commands. The following commands are dangerous if copied incorrectly.

`grep -v ‘NOPASSWD’ /etc/sudoers > tmpfile && mv tmpfile /etc/sudoers – Remove all lines containing the NOPASSWD tag from the first file`

`grep -v ‘NOPASSWD’ /etc/sudoers.d > tmpfile && mv tmpfile /etc/sudoers.d – Remove all lines containing the NOPASSWD tag from the second file`

### 4. Configure login definitions

Edit login definitions so that failed login attempts are logged and a home directory is automatically created for every new user.

`nano /etc/login.defs – Edit the files`

`Find FAILLOG_ENAB and make sure it is set to yes`

`Find CREATE_HOME and make sure it is set to yes`

### 5. Install/update packages and necessary repositories

It is important to keep packages and repositories updated.

`yum update – Update packages`

`yum install epel-release – Install the EPEL repository`

`yum repolist – Verify the installation of EPEL`

### 6. Set up optimal CLI environment with tmux

Multiple terminal windows can be opened on a CLI by using tmux. This is a powerful tool that makes monitoring for suspicious activity much easier.

`yum install tmux – Install tmux`

`tmux – Run tmux`

>Ctrl+b ? `– List tmux keybindings (get familiar with them), press Up or Down to scroll, press q to exit`

>Ctrl+b & `– Kills the window and exits tmux if no new windows were created`

i. Continuously monitor processes/services in a separate tmux session

`tmux new -s pmonitor – Creates a new tmux session called pmonitor (short for process monitor)`

`yum install htop – Install htop`

`htop – Run htop`

>Ctrl+b d `– Detach from the session`

ii. Continuously monitor network traffic in a separate tmux session

`tmux new -s nmonitor – Creates a new tmux session called nmonitor (short for network monitor)`

`yum install nethogs – Install nethogs`

`nethogs – Run nethogs`

>Ctrl+b d `– Detach from the session`

iii. Continuously monitor important logs in a separate tmux session

`tmux new -s lmonitor – Creates a new tmux session called lmonitor (short for log monitor)`

Either do this:

`yum --enablerepo="epel" install multitail – Install MultiTail`

`multitail /var/log/secure /var/log/messages /var/log/faillog – Run MultiTail with the necessary logs`

>Ctrl+b d `– Detach from the session`

Or do this:

`tail -f /var/log/secure – Run followed tail on secure`

`In a new window, tail -f /var/log/messages – Run followed tail on messages`

`In a new window, tail -f /var/log/faillog – Run followed tail on faillog`

>Ctrl+b d `– Detach from the session`

`tmux list-sessions – List all the tmux sessions to ensure they are all running`

`tmux attach -t <session_name> – Opens a tmux session given session name`

### 7. Stop/disable unnecessary processes

Processes that are confirmed to be obstructive or harmful for the system or service should be killed.

`kill -9 <PID> – Kill a process given the PID`

Processes that are confirmed to be nonessential for the functionality of the system or service may be terminated.

`kill <PID> – Terminate a process given the PID`

Processes that access a particular port or file system when they are not supposed to may be killed as well.

`fuser -km <file_system> – Kill processes that access the given directory or file in any way`

`fuser -k <port_number>/tcp – Kill processes that access the given TCP port in any way`

### 8. Install and use UFW

A firewall will be necessary for preventative network security, and UFW fulfills this role.

`yum install ufw – Install UFW`

`ufw enable – Enable UFW (This may immediately stop traffic through all ports, be sure to manually allow required ports)`

`ufw allow <port_number>/tcp – Allow traffic through a particular TCP port (needed for dedicated service)`

`ufw deny <port_number>/tcp – Deny traffic through a particular TCP port (needed for dedicated service)`

`sudo ufw allow from <IP> to any – Allow traffic through a particular IP address`

`sudo ufw deny from <IP> to any – Deny traffic through a particular IP address`

`nano /etc/ufw/before.rules – Edit the file to block IP addresses regardless of order of rules, after the line # End required lines, append lines like -A ufw-before-input -s <IP_or_Subnet> -j DROP`

`ufw reload – Reload UFW after editing a configuration file`

`ufw status – Show a list of opened and limited IP addresses and ports`

### 9. Lock down critical files

There are some extremely important files that should have protection against tamper and removal.

`chattr +i /etc/services – Make the services file immutable`
`chattr +i /etc/passwd – Make the passwd file immutable`
`chattr +i /etc/shadow – Make the shadow file immutable`
`chattr -i <filename> – Remove the immutable attribute for a given filename (remember to add the attribute back after completing file modification`)
`chattr +a /var/log/secure – Make secure append-only`
`chattr +a /var/log/messages – Make messages append-only`
`chattr +a /var/log/faillog – Make faillog append-only`
`chattr -a <filename> – Remove the append-only attribute for a given filename (not to be used for log files)`
`lsattr – List files that have modified attributes`

Also, it would help to require confirmation for file removal to prevent accidental deletions.

`alias rm="rm -i" – Set the alias of the remove command to require confirmation`

### 10. Adjust NTP to time.gov

It may prove beneficial to synchronize the machine time with a centralized NTP server.

`yum install ntp – Install NTP`

`systemctl start ntpd – Start NTP client`

`systemctl enable ntpd – Enable NTP client on startup`

`systemctl status ntpd – Check NTP client status`

`ntupdate -u time.nist.gov – Update target NTP server to time.nist.gov`

`systemctl restart ntpd – Restart NTP client`

`timedatectl – Check time and date details such as whether time and date are NTP synchronized`

`hwclock -systohc – Write system clock time to hardware clock`

## Possible Injects

### 11. Update password policy

It may be requested to use a standardized password policy for machines and services.

i. For the system (CentOS 7)

`cp /etc/security/pwquality.conf /etc/security/pwquality.conf.old – Backup pwquality.conf`

`cp /etc/pam.d/system-auth /etc/pam.d/system-auth.old – Backup system-auth`

`nano /etc/security/pwquality.conf – Edit pwquality.conf`

Here is an example configuration, adjust accordingly:

> `difok = 3` – Requires minimum of 3 different characters in a new password when compared against the current password

> `minlen = 8` – Requires password length of a minimum of 8 characters

> `ucredit = -1` – Requires at least 1 upper case letter

> `lcredit = -1` – Requires at least 1 lower case letter

> `dcredit = -1` – Requires at least 1 number

> `ocredit = -1` – Requires at least 1 non-alphanumeric character

> `minclass = 2` – Requires at least 2 classes or characters: (upper, lower, digit and other)

> `maxrepeat = 3` – Rejects passwords that have occurrences of 4 or more repeating identical characters

> `maxclassrepeat = 2` – Rejects passwords that have 3 or more consecutive characters of the same class

`nano /etc/pam.d/system-auth – Edit system-auth`

`password requisite pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type= enforce_for_root – Append enforce_for_root to the end of this line to force root and users with sudo privileges to comply with the password policy`

`pwscore – Test password policy by purposely entering passwords that should fail, the output for such cases should begin with “Password quality check failed”`

If requested, expire all user passwords

`passwd -e <username> – Expire password for a particular user`

ii. For the dedicated service (Splunk 8.1.1)

`nano authentication.conf – Edit authentication.conf, must be in $SPLUNK_HOME`

Under [splunk_auth] stanza, adjust the following variables accordingly:

>minPasswordLength

>minPasswordUppercase

>minPasswordLowercase

>minPasswordSpecial

>minPasswordDigit

Only local user accounts will be affected, contact an AD administrator to apply password policy for LDAP users.

### 12. Add, enable, and unlock users/groups

It may be requested to create new users and/or groups. It may also be requested to regain access to user accounts.

i. For the system (CentOS 7)

`adduser <username> – Create a user with the given username`

`passwd <username> - Create a password for given user`

`groupadd <group_name> - Create a group with the given group name`

`usermod -aG <group_name> <username> - Add a user to a group`

`passwd -a <username> wheel – Safely grant a user sudo privileges `

`passwd -u <username> - Unlock a user account`

`chage -E-1 <username> - Enable a user account`

ii. For the dedicated service (Splunk 8.1.1)

`./splunk add user <username> -password <password> -auth admin:<admin_password> - Add a user with given username`

`./splunk edit user <username> -locked-out false -auth admin:<admin_password> – Unlock user account`

`./splunk add user <username> -password <password> -role admin -auth admin:<admin_password> - Add an administrator with given username`

`./splunk edit user <username> -role <role_name> - Change the role of a user to a given role (Multiple roles can be assigned using multiple -role <role_name> arguments)`

`nano $SPLUNK_HOME/etc/system/local/authorize.conf – Create and edit roles by editing authorize.conf and making the appropriate changes`

Unlocking an administrator account is an extensive process that requires stopping the service, so it would be more appropriate to delete that account and replace it with a new one.

### 13. Install and use ClamAV

It may be requested to use antivirus software, such as ClamAV.

Either do this (Build ClamAV from scratch):

`yum -y install clamav-server clamav-data clamav-update clamav-filesystem clamav clamav-scanner-systemd clamav-devel clamav-lib clamav-server-systemd – Install all required tools for building ClamAV`

`setsebool -P antivirus_can_scan_system 1 – Configure SELinux part 1`

`setsebool -P clamd_use_jit 1 – Configure SELinux part 2`

`getsebool -a | grep antivirus – Configure SELinux part 3, make sure the output matches:`

`>antivirus_can_scan_system --&gt; on`

`>antivirus_use_jit --&gt; off`

`sed -i -e "s/^Example/#Example/" /etc/clamd.d/scan.conf – Configure ClamAV part 1`

`nano /etc/clamd.d/scan.conf – Configure ClamAV part 2, Uncomment #LocalSocket /var/run/clamd.scan/clamd.sock`

`sudo sed -i -e "s/^Example/#Example/" /etc/freshclam.conf – Configure ClamAV part 3`

`freshclam – Configure ClamAV part 4`

Or do this:

`yum install clamav – Install ClamAV`

`clamscan / -vr --move=/VIRUS | tee <filename>.txt – Perform a scan on the current directory, automatically quarantine infected files, and save results to text file (Recommended to do this in a tmux session since this could take a while)`

### 14. Install and use Nmap

It may be requested to use a port scanner, such as Nmap.

yum install nmap – Install Nmap

`nmap -v -A <IP> > <filename>.txt – Scan the given IP address and save detailed results to a text file`

`nmap <lower_IP>-<upper_IP> > <filename>.txt – Scan the given range of IP addresses and save results to a text file (Recommended to do this in a tmux session since this could take a while)`

`nmap <IP>/<IP_bits> > <filename>.txt – Scan a subnet and save results to a text file, use something similar to ‘X.X.X.0’ for IP and ‘24’ for IP bits to scan all IP addresses with the first 24 bits (Recommended to do this in a tmux session since this could take a while)`

`nmap -p <port> <IP> > <filename>.txt – Scan a port associated with the provided IP and save results to a text file`

`nmap -p <lower_port>-<upper_port> <IP> > <filename>.txt – Scan a range of ports associated with the provided IP and save results to a text file`

###15. Enable/disable SSH

It may be requested to enable or disable SSH. Remember to allow, limit, deny, and change the SSH port in UFW when needed.

`yum –y install openssh-server openssh-clients – Install relevant services for SSH`

`systemctl start sshd – Start SSH daemon`

`systemctl stop sshd – Stop SSH daemon`

`nano /etc/ssh/sshd_config – Edit the configuration file for SSH (This will be needed for changing SSH port number and disabling root login)`

`service sshd restart – Restart SSH daemon after making changes to configuration file`


## What To Do If...
### 16. There is a rogue SSH connection

A rogue SSH user may be detected by monitoring the logs called “secure” and “faillog”, as well as monitoring network traffic.

• Kill process (See Section 7)
• Block IP address (See Section 8)

### 17. There is a malicious process running

A malicious process may be detected by monitoring the log called “secure”, as well as monitoring processes.

• Kill process (See Section 7)

###18. There is suspicious network traffic

Suspicious network traffic may be detected by monitoring the log called “secure”, as well as monitoring network traffic. There may even be suspicious traffic detected through Nmap.

• Block port if applicable (See Section 8)
• Block IP address (See Section 8)

### 19. A vital component is nonfunctional

A service is nonfunctional if it is not using any resources at all or if it is not appearing at all while monitoring processes.

i. For the system (CentOS 7)

• Restart service with systemctl

ii. For the dedicated service (Splunk 8.1.1)

• Restart Splunk

### 20. Malware has been detected

Malware may be detected through the use of ClamAV (See section 13). If using the command provided, simply navigate to the VIRUS directory, decide which files are worth keeping, if any, and delete every other file. 
